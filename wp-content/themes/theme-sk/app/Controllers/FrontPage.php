<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{

    public function header() {

        $dataHeader = (object) array( 
            'bgcolor' => get_field('couleur_du_background'),
            'imglogo' => get_field('logo_principal'),
            'texteprincipal' => get_field('texte_principal'),
            'textesecondaire' => get_field('paragraphe_de_presentation'),
            'liens_bulles' => get_field('liens_bulles'),
            'bgsvg' => get_field('header_svg'),
        );

        return $dataHeader;
     }


     public function bloc1() {

        $dataBloc1 = (object) array( 
            'mockup' => get_field('mockup'),
        );

        return $dataBloc1;
     }

     public function bloc3() {

        $dataBloc3 = (object) array( 
            'background' => get_field('background'),
            'arrow' => get_field('arrow'),
            'icones' => get_field('icones_numeros'),
        );

        return $dataBloc3;
     }

     public function bloc2() {

        $dataBloc2 = (object) array( 
            'mockup' => get_field('mockup_bloc3'),
        );

        return $dataBloc2;
     }

     public function works() {
        $args = array(
            'post_type'   => 'realisations'
          );
           
          $dataWorks = get_posts( $args );

        return $dataWorks;
     }

     public static function getTechnicText() {
        get_field('competences_et_techniques');
     }

   
     

}
