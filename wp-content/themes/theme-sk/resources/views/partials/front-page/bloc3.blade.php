<div class="sk-c-Bloc3">

    <p class="sk-c-Bloc3-title">
        Découvrez mes projets
    </p>

    <div class="sk-c-Slider">

            @foreach ($works as $work)

            @php 
              $workFields = get_fields($work->ID);
              $workPermalink = get_permalink($work->ID);
            @endphp


                <a href="{{ $workPermalink }}">
                <div id="{{ $work->ID }}" data-url="{{ $workPermalink }}" class="sk-c-Slider-slide" style="background-image: url('{{ $workFields['background_rea']['url'] }}')">
                        

                        <div class="sk-c-Slider-slide-overlay"></div>
                        
                        <img src="{{ $workFields['mockup_rea']}}" class="sk-c-Slider-slide-mockup">

                        <div class="sk-c-Slider-slide-projectdesc">
                                <div class="sk-c-Slider-slide-projectdesc-title">{{ $work->post_title }}</div>
                                <div class="sk-c-Slider-slide-projectdesc-desc">{{ $workFields['tags_rea'] }}</div>
                        </div>	
                </div>
                </a>

            @endforeach

        </div>
</div>