<div class="sk-c-Bloc1">
    <h2>Développeur créatif</h2>
    <div class="sk-c-Bloc1-stroke"></div>
    <p>Fan de développement web, mobile et de jeux, j’ai pu apprendre personnellement et grâce à mes formations de nombreux langages. Mon but est de me perfectionner afin de devenir développeur full-stack. Voici les domaines dans lesquels j'ai acquis des connaissances : </p>
    <button class="sk-c-Bloc1-button">Découvrir mes projets</button>
    <div class="anim1" id="bm"></div>
    <img src="{{ $bloc1->mockup['url'] }}" class="imgmockup">

    <div class="bkg_page2" style="background-image: url({{ $bloc3->background['url'] }})">
        <div class="sk-c-Bloc1-projects">
            <div class="sk-c-Bloc1-projects-1dream">
                <p class="sk-c-Bloc1-projects-bold">Vous<p> <p class="sk-c-Bloc1-projects-light">rêvez</p>
                <img src="{{ $bloc3->icones['icone_1']}}" class="sk-c-Bloc1-projects-iconnumber">
            </div>
            <div class="sk-c-Bloc1-projects-2imagine">
                <p class="sk-c-Bloc1-projects-bold">Nous</p> <p class="sk-c-Bloc1-projects-light">imaginons</p>
                <img src="{{ $bloc3->icones['icone_2']}}" class="sk-c-Bloc1-projects-iconnumber">
            </div>
            <div class="sk-c-Bloc1-projects-3create">
                <p class="sk-c-Bloc1-projects-bold">Je</p> <p class="sk-c-Bloc1-projects-light">conçois</p>
                <img src="{{ $bloc3->icones['icone_3']}}" class="sk-c-Bloc1-projects-iconnumber">
            </div>
        </div>
    <img src="{{ $bloc3->background['url'] }}" class="bkg_page2-inner"></div>
</div>