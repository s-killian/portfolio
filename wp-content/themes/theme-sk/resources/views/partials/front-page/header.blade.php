<div class="sk-c-Header">

    <div class="sk-c-Header-container-bgsvg-parallax">
        <img src="{{ $header->bgsvg }}" class="sk-c-Header-container-bgsvg">
    </div>

        <div class="sk-c-Header-container">
    
            <img src="{{ $header->imglogo['url'] }}" class="sk-c-Header-container-logo">
        
            <h2>{{ $header->texteprincipal }}</h2>
            <p>{{ $header->textesecondaire }}</p>

            <span class="sk-c-Stroke sk-c-Stroke-strokeleft"></span>
            <a href="#">
                <span class="sk-c-Circle sk-c-Circle-circleleft">
                <p>{{$header->liens_bulles['bulle_gauche']}}</p>
                </span>
            </a>
            <span class="sk-c-Stroke sk-c-Stroke-strokemiddle"></span>
            <span class="sk-c-Circle sk-c-Circle-circlemiddle">
                <p>{{$header->liens_bulles['bulle_milieu']}}</p>
            </span>
            <span class="sk-c-Stroke sk-c-Stroke-strokeright"></span>
            <span class="sk-c-Circle sk-c-Circle-circleright">
                <p>{{$header->liens_bulles['bulle_droite']}}</p>
            </span>
        </div>


</div>