{{--
  Template Name: Custom Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.common.page-header')
    @include('partials.common.content-page')
  @endwhile
@endsection
