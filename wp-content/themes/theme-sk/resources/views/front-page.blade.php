@extends('layouts.app')

@section('content')
    @include('partials.front-page.firstAnimation')
    @include('partials.front-page.header')
    @include('partials.front-page.bloc1')
    @include('partials.front-page.bloc2')
    @include('partials.front-page.bloc3')
@endsection
