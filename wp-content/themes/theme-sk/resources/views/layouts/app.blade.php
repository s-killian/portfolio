<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.common.head')
  <body @php body_class() @endphp>
  <main id="swup">
      <div class="content">
          @yield('content')
      </div>
      @php do_action('get_footer') @endphp
    @include('partials.common.footer')
    @php wp_footer() @endphp

  </main>
  </body>
</html>
