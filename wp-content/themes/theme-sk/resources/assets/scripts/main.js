// import external dependencies
import 'jquery';

// Import everything from autoload
// import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import HomeSlider from './classes/HomeSlider';
import FirstAnim from './classes/FirstAnim';
import SingleParallax from './classes/SingleParallax';
import SwupPage from './classes/SwupPage';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,

});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

document.addEventListener('swup:contentReplaced', ()=> {
    new FirstAnim();
    new HomeSlider();
    new SingleParallax();
    new SwupPage();
});
