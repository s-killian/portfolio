import 'jquery';
import Parallax from 'parallax-js';
import 'slick-carousel';
import {TimelineLite} from 'gsap/TweenMax';
import HomeSlider from '../classes/HomeSlider';
import FirstAnim from '../classes/FirstAnim';
import SingleParallax from '../classes/SingleParallax';
import SwupPage from '../classes/SwupPage';

export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    
    new FirstAnim();
    new HomeSlider();
    new SingleParallax();
    new SwupPage();

  },
};
