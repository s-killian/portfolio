
import 'jquery';
import Parallax from 'parallax-js';

export default class SingleParallax{
    constructor() {
        this.initsEls();
        this.initEvents();
    }
    initsEls() {
        
    }
    initEvents() { 
        this.launchParralax();
    }

    launchParralax() {
        var scene = document.querySelector('.sk-c-Single-header-content');
        new Parallax(scene, {
          invertX:false,
          invertY:false,
        });
    }
    
}