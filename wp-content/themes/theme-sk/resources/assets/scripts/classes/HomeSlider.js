
import 'jquery';
import 'slick-carousel';

export default class HomeSlider{
    constructor() {
        this.initsEls();
        this.initEvents(); 
    }
    initsEls() {
        
    }
    initEvents() { 
        this.slickRea();
    }

    slickRea() {

        $('.sk-c-Slider').slick({
            centerMode: true,
                  speed: 500,
                  arrows: false,
                  dots: true,
                  draggable: false,
            accessibility: true,
            slidesToShow: 3,    
            variableWidth: true,
            
            responsive: [
              {
                breakpoint: 1240,
                settings: {
                  slidesToShow: 2,
                },
              },
              {
                breakpoint: 875,
                settings: {
                  slidesToShow: 1,
                },
              },

              
            ],
      
          });
        
    }

    slideOverlay() {
        $( '.sk-c-Slider-slide' ).hover(
            function() {
              $(this).find('.sk-c-Slider-slide-overlay').css('opacity', '0');
              $(this).find('.sk-c-Slider-slide-mockup').css('transform', 'scale(1.1) translate(-50%, -50%)');
              
            }, function() {
              $(this).find('.sk-c-Slider-slide-overlay').css('opacity', '1');
              $(this).find('.sk-c-Slider-slide-mockup').css('transform', 'scale(1) translate(-50%, -50%)');
          });
    }
}