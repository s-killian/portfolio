
import 'jquery';
import {TimelineLite} from 'gsap/TweenMax';

export default class FirstAnim{
    constructor() {
        this.initsEls();
        this.initEvents(); 
    }
    initsEls() {
        
    }
    initEvents() { 
        this.firstAnimation();
    }

    firstAnimation() {
            let tl = new TimelineLite();
            tl.to('.sk-c-FirstAnimation-animation1-logo', 1.5, {scale:1, transformOrigin:'0% 100%', opacity: 1})
              .to('.sk-c-FirstAnimation-animation1', 1.4, {scaleX:0, scaleY:0})
              .to('.sk-c-FirstAnimation-animation1-logo', 1.4, {opacity: 0}, '-=1.4');
            tl.to('.sk-c-FirstAnimation-animation1', 1, {y:'-100%'}).delay(1.5);
            tl.to('.sk-c-FirstAnimation-animation2', 1.5, {x:'-100%'}, '-=0');
            tl.from('.sk-c-Header-container', 2, {scaleX:0, scaleY:0, opacity:0}, '-=1.5')
              .to('.sk-c-Stroke-strokeleft', 0.7, {height: '100px'}, '-=1.5')
              .to('.sk-c-Stroke-strokemiddle', 0.7, {height: '100px'}, '-=1')
              .to('.sk-c-Stroke-strokeright', 0.7, {height: '100px'}, '-=0.4')
    }
    
}