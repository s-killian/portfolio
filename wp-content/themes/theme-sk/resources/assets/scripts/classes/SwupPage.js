import Swupjs from 'swupjs';
import {TimelineMax, TweenLite, Power4} from 'gsap';
export default class SwupPage{
    constructor() {
        this.initsEls();
        this.initEvents(); 
    }
    initsEls() {
        
        this.options = {
            animations: {
                '*': {
                    in: function(next){
                        document.querySelector('#swup').style.opacity = 0;
                        TweenLite.to(document.querySelector('#swup'), .5, {
                            opacity: 1,
                            onComplete: next,
                        });
                    },
                    out: function(next){
                        document.querySelector('#swup').style.opacity = 1;
                        TweenLite.to(document.querySelector('#swup'), .5, {
                            opacity: 0,
                            onComplete: next,
                        });
                    },
                },

                '*>realisations': {
                    in: function(next){
                        var tl = new TimelineMax();
                        tl
                        .fromTo($('.sk-c-Single-header-content'), 1.5, {scale:0}, {scale:1})
                    
                    },
                    out: function(next){
                        var tl = new TimelineMax();
                        tl
                        .to($('.sk-c-Single-content-inner'), 1.5, {
                            scale: 0,
                            ease: Power4.easeInOut,
                            onComplete: next,
                        })
                    },
                },
            },
            cache: true,
            preload: false,
            debugMode: true,
            animateScroll: false,
        };
    }
    initEvents() { 
        this.launchSwup();
    }

    launchSwup() {
        const swupjs = new Swupjs(this.options);
    }
}