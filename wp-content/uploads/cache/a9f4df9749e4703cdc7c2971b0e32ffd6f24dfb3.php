<div class="sk-c-Bloc3">

    <p class="sk-c-Bloc3-title">
        Découvrez mes projets
    </p>

    <div class="sk-c-Slider">

            <?php $__currentLoopData = $works; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $work): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php 
              $workFields = get_fields($work->ID);
              $workPermalink = get_permalink($work->ID);
            ?>


                <a href="<?php echo e($workPermalink); ?>">
                <div id="<?php echo e($work->ID); ?>" data-url="<?php echo e($workPermalink); ?>" class="sk-c-Slider-slide" style="background-image: url('<?php echo e($workFields['background_rea']['url']); ?>')">
                        

                        <div class="sk-c-Slider-slide-overlay"></div>
                        
                        <img src="<?php echo e($workFields['mockup_rea']); ?>" class="sk-c-Slider-slide-mockup">

                        <div class="sk-c-Slider-slide-projectdesc">
                                <div class="sk-c-Slider-slide-projectdesc-title"><?php echo e($work->post_title); ?></div>
                                <div class="sk-c-Slider-slide-projectdesc-desc"><?php echo e($workFields['tags_rea']); ?></div>
                        </div>	
                </div>
                </a>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
</div>