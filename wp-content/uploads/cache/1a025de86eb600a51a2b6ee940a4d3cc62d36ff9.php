<div class="sk-c-Bloc2">
    <div class="sk-c-Bloc2-mockup">
        <img src="<?php echo e($bloc2->mockup); ?>" class="sk-c-Bloc2-mockup-inner">
    </div>
    
    <div class="sk-c-Bloc2-content">

        <div class="sk-c-Bloc2-content-title">
                <h2>PLUSIEURS DOMAINES, PLUSIEURS  LANGAGES,</h2>
                <h1>UN SEUL OBJECTIF : CONCEVOIR</h1>
        </div>

        <div class="sk-c-Bloc2-content-container">

            <div class="sk-c-Bloc2-content-block">
                <div class="sk-c-Bloc2-content-block-title">
                SUR PLUSIEURS SUPPORTS
                </div>
                <div class="sk-c-Bloc2-content-block-text">
                Passioné par la développement multi-supports, je souhaite me spécialiser sur la partie web et mobile et découvrir de nouvelles choses chaque jour !
                </div>
                <div class="sk-c-Bloc2-content-block-languages">
                    HTMl - CSS - JS - PHP - REACT NATIVE - WORDPRESS - PRESTASHOP
                </div>
            </div>

            <div class="sk-c-Bloc2-content-block">
                <div class="sk-c-Bloc2-content-block-title">
                    AVEC UNE MÉTHODOLOGIE
                </div>
                <div class="sk-c-Bloc2-content-block-text">
                    Maintenant que j’ai découvert plusieurs langages, je travaille  pour améliorer ma façon de travailler et découvrir les meilleurs pratiques du développement.
                </div>
                <div class="sk-c-Bloc2-content-block-languages">
                    GIT - SAGE - BLADE - WEBPACK - COMPONENTS - CONTROLLERS
                </div>
            </div>

        </div>
    </div>
</div>