<article <?php post_class() ?>>
  <header>
    <h1 class="entry-title" id="post_title"><?php echo get_the_title(); ?></h1>
    <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </header>
  <div class="entry-content">
      <p id="txt_presentation"><?php echo e(get_field('presentation')); ?></p>
      <p id="txt_technique"><?php echo e(get_field('competences_et_techniques')); ?></p>
      <p id="url"><?php echo e(get_field('url')); ?></p>
      <img src="<?php echo e(get_field('mockup_big')); ?>" alt="" id="mockup">
  </div>
  <footer>
    <?php echo wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

  </footer>
  <?php comments_template('/partials/comments.blade.php') ?>
</article>
