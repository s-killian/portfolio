<!doctype html>
<html <?php echo get_language_attributes(); ?>>
  <?php echo $__env->make('partials.common.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <body <?php body_class() ?>>
  <main id="swup">
      <div class="content">
          <?php echo $__env->yieldContent('content'); ?>
      </div>
      <?php do_action('get_footer') ?>
    <?php echo $__env->make('partials.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php wp_footer() ?>

  </main>
  </body>
</html>
