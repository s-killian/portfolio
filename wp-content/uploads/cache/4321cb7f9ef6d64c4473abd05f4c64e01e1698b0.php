<article class="sk-c-Single">

  <header class="sk-c-Single-header" style="background-image: url('<?php echo e(get_field('background_big')); ?>')">
    <div class="sk-c-Single-header-overlay"></div>
      <div class="sk-c-Single-header-content">
          <h1 class="sk-c-Single-header-content-title" id="post_title" data-depth="0"><?php echo get_the_title(); ?></h1>
          <h1 class="sk-c-Single-header-content-title-opaque" id="post_title" data-depth="0.4"><?php echo get_the_title(); ?></h1>
      </div>
        
  </header>

  <div class="sk-c-Single-content">

      <?php
      $prev_post = get_previous_post();
      $prev_post_url = get_permalink($prev_post->ID);

      $next_post = get_next_post();
      $next_post_url = get_permalink($next_post->ID);
      ?>
      

      <div class="sk-c-Single-content-inner" style="background-image: url('<?php echo e(get_field('background_big')); ?>')">

        <?php if($prev_post !== ''): ?>
          <a href="<?php echo e($prev_post_url); ?>" data-swup-transition="realisations">
              <img src="<?= App\asset_path('images/previous.svg'); ?>" class="sk-c-Single-content-inner-arrowLeft">
          </a> 
        <?php endif; ?>

        <?php if($next_post !== ''): ?>
          <a href="<?php echo e($next_post_url); ?>" data-swup-transition="realisations">
            <img src="<?= App\asset_path('images/previous.svg'); ?>" class="sk-c-Single-content-inner-arrowRight">
          </a>
        <?php endif; ?>

        <div class="sk-c-Single-content-inner-fakeDiv">

          <div class="sk-c-Single-content-inner-top">
            <p id="url"><?php echo e(get_field('problematique')); ?></p>
            <div class="sk-c-Single-content-inner-top-tags">
                <?php echo e(the_field('tags_rea')); ?> 
                <a href="<?php echo e(get_field('url')); ?>" target="_blank">
                  <span>voir le site</span>
                </a>
          </div>
          </div>

        <div class="sk-c-Single-content-inner-middle">

            <div class="sk-c-Single-content-inner-middle-top">
              <div class="sk-c-Single-content-inner-middle-top-left">
                <h2>Description du projet</h2>
                <p id="txt_presentation"><?php echo e(get_field('presentation')); ?></p>
              </div>
              <div class="sk-c-Single-content-inner-middle-top-right">
                <h2>Caractéristiques du projet</h2>
                <p id="txt_technique"><?php echo e(the_field('competences_et_techniques')); ?></p>
              </div>
            </div>

            <div class="sk-c-Single-content-inner-middle-bottom">
                <img src="<?php echo e(get_field('mockup_grand')); ?>" alt="" id="mockup">
            </div>
          
        </div>

      </div>

      </div>
  </div>
</article>
