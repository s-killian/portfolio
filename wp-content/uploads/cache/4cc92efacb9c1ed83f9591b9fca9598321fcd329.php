<div class="sk-c-Header">

    <div class="sk-c-Header-container-bgsvg-parallax">
        <img src="<?php echo e($header->bgsvg); ?>" class="sk-c-Header-container-bgsvg">
    </div>

        <div class="sk-c-Header-container">
    
            <img src="<?php echo e($header->imglogo['url']); ?>" class="sk-c-Header-container-logo">
        
            <h2><?php echo e($header->texteprincipal); ?></h2>
            <p><?php echo e($header->textesecondaire); ?></p>

            <span class="sk-c-Stroke sk-c-Stroke-strokeleft"></span>
            <a href="#">
                <span class="sk-c-Circle sk-c-Circle-circleleft">
                <p><?php echo e($header->liens_bulles['bulle_gauche']); ?></p>
                </span>
            </a>
            <span class="sk-c-Stroke sk-c-Stroke-strokemiddle"></span>
            <span class="sk-c-Circle sk-c-Circle-circlemiddle">
                <p><?php echo e($header->liens_bulles['bulle_milieu']); ?></p>
            </span>
            <span class="sk-c-Stroke sk-c-Stroke-strokeright"></span>
            <span class="sk-c-Circle sk-c-Circle-circleright">
                <p><?php echo e($header->liens_bulles['bulle_droite']); ?></p>
            </span>
        </div>


</div>